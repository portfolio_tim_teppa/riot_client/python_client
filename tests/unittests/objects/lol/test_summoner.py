import unittest

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.summoner import Summoner
from tests.common.known import KNOWN_SUMMONER
from tests.unittests.base_test_case import BaseTestCase


class TestByRiotId(BaseTestCase):
    async def test_known_summoner(self) -> None:
        region = Region.EUW1
        summoner = await Summoner.by_account(account_id=KNOWN_SUMMONER.account_id, region=region)
        self.assertEqual(summoner, KNOWN_SUMMONER)


class TestBySummonerId(BaseTestCase):
    async def test_known_summoner(self) -> None:
        region = Region.EUW1
        summoner = await Summoner.by_id(summoner_id=KNOWN_SUMMONER.id, region=region)
        self.assertEqual(summoner, KNOWN_SUMMONER)


class TestBySummonerPuuid(BaseTestCase):
    async def test_known_summoner(self) -> None:
        region = Region.EUW1
        summoner = await Summoner.by_puuid(summoner_puuid=KNOWN_SUMMONER.puuid, region=region)
        self.assertEqual(summoner, KNOWN_SUMMONER)


if __name__ == "__main__":
    unittest.main()
