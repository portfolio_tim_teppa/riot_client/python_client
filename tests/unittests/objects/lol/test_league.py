import unittest

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.constants import Queue
from riot_client.objects.lol.league import League
from tests.common.known import (CHALLENGER_LEAGUE, GRAND_MASTER_LEAGUE,
                                KNOWN_LEAGUE, MASTER_LEAGUE)
from tests.unittests.base_test_case import BaseTestCase


class TestById(BaseTestCase):
    async def test_known_league(self) -> None:
        region = Region.EUW1
        league = await League.by_id(league_id=KNOWN_LEAGUE.id, region=region)
        self.assertEqual(league, KNOWN_LEAGUE)


class TestChallenger(BaseTestCase):
    async def test_known_league(self) -> None:
        region = Region.EUW1
        league = await League.challenger(queue=Queue.RANKED_SOLO_5X5, region=region)
        self.assertEqual(league, CHALLENGER_LEAGUE)


class TestGrandMaster(BaseTestCase):
    async def test_known_league(self) -> None:
        region = Region.EUW1
        league = await League.grand_master(queue=Queue.RANKED_SOLO_5X5, region=region)
        self.assertEqual(league, GRAND_MASTER_LEAGUE)


class TestMaster(BaseTestCase):
    async def test_known_league(self) -> None:
        region = Region.EUW1
        league = await League.master(queue=Queue.RANKED_SOLO_5X5, region=region)
        self.assertEqual(league, MASTER_LEAGUE)


if __name__ == "__main__":
    unittest.main()
