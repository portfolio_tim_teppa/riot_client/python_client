import unittest
from riot_client.objects.common.constants import Region
from riot_client.objects.lol.status.platform_data import PlatformData
from tests.unittests.base_test_case import BaseTestCase


class TestGet(BaseTestCase):
    async def test_request(self) -> None:
        platform_data = await PlatformData.get(region=Region.EUW1)


if __name__ == "__main__":
    unittest.main()
