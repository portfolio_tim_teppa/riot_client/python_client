import unittest

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.constants import Division, Queue, Tier
from riot_client.objects.lol.league_entry import LeagueEntry
from tests.common.known import KNOWN_SUMMONER
from tests.unittests.base_test_case import BaseTestCase


class TestBySummoner(BaseTestCase):
    async def test_known_league(self) -> None:
        region = Region.EUW1
        await LeagueEntry.by_summoner(summoner_id=KNOWN_SUMMONER.id, region=region)


class TestFrom(BaseTestCase):
    async def test_known_league(self) -> None:
        region = Region.EUW1
        await LeagueEntry.from_(
            queue=Queue.RANKED_FLEX_SR,
            tier=Tier.BRONZE,
            division=Division.IV,
            region=region
        )


if __name__ == "__main__":
    unittest.main()
