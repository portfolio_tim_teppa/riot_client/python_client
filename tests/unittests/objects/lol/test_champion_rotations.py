import unittest

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.champion_rotations import ChampionRotations
from tests.unittests.base_test_case import BaseTestCase


class TestGet(BaseTestCase):
    async def test_known_account(self) -> None:
        region = Region.EUW1
        await ChampionRotations.get(region=region)


if __name__ == "__main__":
    unittest.main()
