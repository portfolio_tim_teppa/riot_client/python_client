import unittest

from tests.common.known import KNOWN_ACCOUNT
from tests.unittests.base_test_case import BaseTestCase

from riot_client.objects.account import Account
from riot_client.objects.common.constants import Continent


class TestByRiotId(BaseTestCase):
    async def test_known_account(self) -> None:
        continent: Continent = Continent.EUROPE
        account = await Account.by_riot_id(
            game_name=KNOWN_ACCOUNT.game_name,
            tag_line=KNOWN_ACCOUNT.tag_line,
            continent=continent
        )
        self.assertEqual(account, KNOWN_ACCOUNT)


class TestByPuuid(BaseTestCase):
    async def test_known_account(self) -> None:
        continent: Continent = Continent.EUROPE
        account = await Account.by_puuid(puuid=KNOWN_ACCOUNT.puuid, continent=continent)
        self.assertEqual(account, KNOWN_ACCOUNT)


if __name__ == "__main__":
    unittest.main()
