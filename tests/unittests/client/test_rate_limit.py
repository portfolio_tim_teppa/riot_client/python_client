from datetime import datetime, timedelta
import unittest

from riot_client.client.rate_limit import RateLimit, RateLimits
from riot_client.client.requests_history import RequestsHistory


class TestRateLimitWasReached(unittest.TestCase):
    def test_no_request(self) -> None:
        """With no request, the limit should not be reached"""
        rate_limit = RateLimit(
            time_interval=timedelta(seconds=120),
            amount_of_requests=10
        )
        requests_history = RequestsHistory(
            time_between_refresh=rate_limit.time_interval
        )
        self.assertFalse(rate_limit.was_reached(requests_history))

    def test_limit_was_not_reached(self) -> None:
        """The number of requests in given timelapse is notreached"""
        rate_limit = RateLimit(
            time_interval=timedelta(days=1),
            amount_of_requests=10
        )
        requests_history = RequestsHistory(
            time_between_refresh=rate_limit.time_interval
        )
        now = datetime.now()
        for i in range(rate_limit.amount_of_requests - 1):
            requests_history.add(now - timedelta(seconds=i))
        self.assertFalse(rate_limit.was_reached(requests_history))

    def test_limit_was_reached(self) -> None:
        """The number of requests in given timelapse is reached"""
        rate_limit = RateLimit(
            time_interval=timedelta(days=1),
            amount_of_requests=10
        )
        requests_history = RequestsHistory(
            time_between_refresh=rate_limit.time_interval
        )
        now = datetime.now()
        for i in range(rate_limit.amount_of_requests):
            requests_history.add(now - timedelta(seconds=i))
        self.assertTrue(rate_limit.was_reached(requests_history))

    def test_only_old_requests(self) -> None:
        """No request in the given timelapse, limit should not be reached"""
        rate_limit = RateLimit(
            time_interval=timedelta(seconds=120),
            amount_of_requests=10
        )
        requests_history = RequestsHistory(
            time_between_refresh=rate_limit.time_interval
        )
        for _ in range(rate_limit.amount_of_requests):
            requests_history.add(datetime.now() - timedelta(days=1))
        self.assertFalse(rate_limit.was_reached(requests_history))


class TestRateLimitsWereReached(unittest.TestCase):
    def test_no_rate_limit(self) -> None:
        """With no rate limit, none of them is reached"""
        rate_limits = RateLimits()
        self.assertFalse(
            rate_limits.were_reached(
                RequestsHistory(timedelta(seconds=120))
            )
        )

    def test_one_rate_limit_reached(self) -> None:
        """If at least one limit is reached, should return True"""
        long_limit = RateLimit(time_interval=timedelta(days=2), amount_of_requests=10)
        short_limit = RateLimit(time_interval=timedelta(seconds=10), amount_of_requests=10)
        rate_limits = RateLimits([short_limit, long_limit])
        requests_history = RequestsHistory(time_between_refresh=timedelta(days=2))
        over_short_limit = datetime.now() - short_limit.time_interval
        for i in range(long_limit.amount_of_requests):
            requests_history.add(over_short_limit - timedelta(seconds=i))
        self.assertTrue(long_limit.was_reached(requests_history))
        self.assertFalse(short_limit.was_reached(requests_history))
        self.assertTrue(rate_limits.were_reached(requests_history))

    def test_all_rate_limits_reached(self) -> None:
        """If all limits are reached, should return True"""
        first_limit = RateLimit(time_interval=timedelta(days=2), amount_of_requests=10)
        second_limit = RateLimit(time_interval=timedelta(days=2), amount_of_requests=10)
        rate_limits = RateLimits([first_limit, second_limit])
        requests_history = RequestsHistory(time_between_refresh=timedelta(days=2))
        now = datetime.now()
        for i in range(first_limit.amount_of_requests):
            requests_history.add(now - timedelta(seconds=i))
        self.assertTrue(first_limit.was_reached(requests_history))
        self.assertTrue(second_limit.was_reached(requests_history))
        self.assertTrue(rate_limits.were_reached(requests_history))


if __name__ == "__main__":
    unittest.main()
