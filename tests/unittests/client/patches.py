from typing import Any
from unittest.mock import patch

from riot_client.client.rate_limit import RateLimits
from riot_client.client.requests_history import RequestsHistory


def patch_client_rate_limits(rate_limits: RateLimits) -> Any:
    return patch("riot_client.client.client.RiotClient.RATE_LIMITS", rate_limits)


def patch_client_requests_history(requests_history: RequestsHistory) -> Any:
    return patch("riot_client.client.client.RiotClient._REQUESTS_HISTORY", requests_history)
