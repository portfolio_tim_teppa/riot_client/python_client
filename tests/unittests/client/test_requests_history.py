from datetime import datetime, timedelta
import unittest

from riot_client.client.requests_history import RequestsHistory


class TestGetRequestsIn(unittest.TestCase):
    def test_empty_timedelta(self) -> None:
        """With a null timedelta, should return an empty set"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        requests_history.add(datetime.now())
        _requests = requests_history.get_requests_in(timedelta(seconds=0))
        self.assertCountEqual({}, _requests)

    def test_empty_history(self) -> None:
        """With an empty history, should return an empty set"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        _requests = requests_history.get_requests_in(timedelta(days=2))
        self.assertCountEqual({}, _requests)

    def test_only_outdated(self) -> None:
        """With only outdated requests, should return an empty set"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        outdating_timedelta = timedelta(seconds=120)
        outdating_threshold = datetime.now() - outdating_timedelta
        for i in range(10):
            requests_history.add(outdating_threshold - timedelta(seconds=i))
        _requests = requests_history.get_requests_in(outdating_timedelta)
        self.assertCountEqual({}, _requests)

    def test_no_outdated(self) -> None:
        """With no outdated requests, should return the whole history"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        now = datetime.now()
        for i in range(10):
            requests_history.add(now - timedelta(seconds=i))
        init_requests_history = requests_history.copy()
        _requests = requests_history.get_requests_in(timedelta(days=1))
        self.assertCountEqual(init_requests_history, _requests)

    def test_both_outdated_and_not(self) -> None:
        """With both outdated requests and not, should only return up-to-date"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        number_of_up_to_date: int = 10
        now = datetime.now()
        for i in range(number_of_up_to_date):
            requests_history.add(now - timedelta(seconds=i))
        outdating_timedelta = timedelta(seconds=120)
        outdating_threshold = datetime.now() - outdating_timedelta
        for i in range(10):
            requests_history.add(outdating_threshold - timedelta(seconds=i))
        _requests = requests_history.get_requests_in(outdating_timedelta)
        self.assertEqual(len(_requests), number_of_up_to_date)


class TestRemoveOutdated(unittest.TestCase):
    def test_empty_timedelta(self) -> None:
        """With a null timedelta, history should stay the same"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        requests_history.add(datetime.now())
        init_requests_history = requests_history.copy()
        requests_history.remove_outdated(timedelta(seconds=0))
        self.assertCountEqual(init_requests_history, requests_history)

    def test_empty_history(self) -> None:
        """With an empty history, it should stay empty"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        requests_history.remove_outdated(timedelta(days=2))
        self.assertEqual(len(requests_history), 0)

    def test_only_outdated(self) -> None:
        """With only outdated requests, history should be empty"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        outdating_timedelta = timedelta(seconds=120)
        outdating_threshold = datetime.now() - outdating_timedelta
        for i in range(10):
            requests_history.add(outdating_threshold - timedelta(seconds=i))
        requests_history.remove_outdated(outdating_timedelta)
        self.assertEqual(len(requests_history), 0)

    def test_no_outdated(self) -> None:
        """With no outdated requests, history should stay the same"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        now = datetime.now()
        for i in range(10):
            requests_history.add(now - timedelta(seconds=i))
        init_requests_history = requests_history.copy()
        requests_history.remove_outdated(timedelta(days=1))
        self.assertCountEqual(init_requests_history, requests_history)

    def test_both_outdated_and_not(self) -> None:
        """With both outdated requests and not, only up-to-date ones should stay"""
        requests_history = RequestsHistory(timedelta(seconds=120))
        number_of_up_to_date: int = 10
        now = datetime.now()
        for i in range(number_of_up_to_date):
            requests_history.add(now - timedelta(seconds=i))
        outdating_timedelta = timedelta(seconds=120)
        outdating_threshold = datetime.now() - outdating_timedelta
        for i in range(10):
            requests_history.add(outdating_threshold - timedelta(seconds=i))
        requests_history.remove_outdated(outdating_timedelta)
        self.assertEqual(len(requests_history), number_of_up_to_date)


if __name__ == "__main__":
    unittest.main()
