from datetime import datetime, timedelta
import unittest
from unittest.mock import patch

from riot_client.client.client import RiotClient
from riot_client.client.rate_limit import RateLimit, RateLimits
from riot_client.client.requests_history import RequestsHistory
from tests.unittests.base_test_case import BaseTestCase
from tests.unittests.client import patches


class TestInit(BaseTestCase):
    def test_requests_history(self) -> None:
        """Assert the _REQUESTS_HISTORY attribute is correctly set"""
        client = RiotClient()
        self.assertEqual(
            client._REQUESTS_HISTORY.time_between_refresh,
            timedelta(seconds=120)
        )

    def test_rate_limits(self) -> None:
        """Assert the RATE_LIMITS attribute is correctly set"""
        client = RiotClient()
        self.assertEqual(len(client.RATE_LIMITS), 2)


class TestRateLimitWasReached(BaseTestCase):
    def test_no_rate_limit(self) -> None:
        """If there is no rate limit, should return False"""
        riot_client = RiotClient()
        with patches.patch_client_rate_limits(RateLimits()):
            self.assertFalse(riot_client.rate_limit_was_reached)

    def test_rate_limit_not_reached(self) -> None:
        """If limits are not reached, should return False"""
        riot_client = RiotClient()
        self.assertFalse(riot_client.rate_limit_was_reached)

    def test_rate_limit_reached(self) -> None:
        """If limits are reached, should return True"""
        riot_client = RiotClient()
        rate_limit = RateLimit(time_interval=timedelta(days=1), amount_of_requests=10)
        requests_history = RequestsHistory(time_between_refresh=timedelta(days=1))
        now = datetime.now()
        for i in range(rate_limit.amount_of_requests):
            requests_history.add(now - timedelta(seconds=i))
        with (
            patches.patch_client_rate_limits(RateLimits([rate_limit])),
            patches.patch_client_requests_history(requests_history),
        ):
            self.assertTrue(riot_client.rate_limit_was_reached)


class TestTimeBeforeNextRequestAvailable(BaseTestCase):
    def test_no_rate_limit(self) -> None:
        """If there is no rate limit, should return 0"""
        riot_client = RiotClient()
        with patches.patch_client_rate_limits(RateLimits()):
            self.assertEqual(riot_client.time_before_next_request_available, 0)

    def test_rate_limit_not_reached(self) -> None:
        """If limits are not reached, should return 0"""
        riot_client = RiotClient()
        self.assertEqual(riot_client.time_before_next_request_available, 0)

    def test_rate_limit_reached(self) -> None:
        """If limits are reached, should return a coherent value"""
        riot_client = RiotClient()
        rate_limit = RateLimit(time_interval=timedelta(days=1), amount_of_requests=10)
        requests_history = RequestsHistory(time_between_refresh=timedelta(days=1))
        now = datetime.now()
        for i in range(rate_limit.amount_of_requests):
            requests_history.add(now - timedelta(seconds=i))
        expected_time_to_wait = rate_limit.time_interval.total_seconds()
        with (
            patches.patch_client_rate_limits(RateLimits([rate_limit])),
            patches.patch_client_requests_history(requests_history),
        ):
            self.assertAlmostEqual(
                riot_client.time_before_next_request_available,
                expected_time_to_wait,
                delta=timedelta(seconds=rate_limit.amount_of_requests).total_seconds()
            )


class TestSendRequest(BaseTestCase):
    def tearDown(self) -> None:
        """Assert the request is added to _REQUESTS_HISTORY"""
        return super().tearDown()

    async def test_no_rate_limit(self) -> None:
        """If there is no rate limit, should return the expected response"""
        riot_client = RiotClient()
        with (
            patch("requests.request") as requests_request,
            patches.patch_client_rate_limits(RateLimits())
        ):
            await riot_client.send_request()
        requests_request.assert_called()
        self.assertEqual(len(RiotClient._REQUESTS_HISTORY), 1)

    async def test_rate_limit_not_reached(self) -> None:
        """If limits are not reached, should return the expected response"""

    async def test_rate_limit_reached(self) -> None:
        """If limits are reached, asyncio.sleep should be called,
        then it should return the expected response
        """


if __name__ == "__main__":
    unittest.main()
