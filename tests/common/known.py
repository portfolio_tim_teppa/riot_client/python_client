from riot_client.objects.account import Account
from riot_client.objects.lol.constants import Queue, Tier
from riot_client.objects.lol.league import League
from riot_client.objects.lol.summoner import Summoner

KNOWN_LEAGUE = League(
    tier=Tier.SILVER,
    leagueId="9e8866cc-72a5-4e20-940d-9383a193dde3",
    queue=Queue.RANKED_SOLO_5X5,
    name="Maokai's Ritualists",
    entries=[]
)

CHALLENGER_LEAGUE = League(
    tier=Tier.CHALLENGER,
    leagueId="ecb1adf1-22ee-3814-a360-b5a7f456bf4e",
    queue=Queue.RANKED_SOLO_5X5,
    name="Nami's Alliance",
    entries=[]
)

GRAND_MASTER_LEAGUE = League(
    tier=Tier.GRANDMASTER,
    leagueId="50b34967-3fff-36f8-8cd9-16f1b9a8b67d",
    queue=Queue.RANKED_SOLO_5X5,
    name="Garen's Archons",
    entries=[]

)

MASTER_LEAGUE = League(
    tier=Tier.MASTER,
    leagueId="98dad3a2-1827-3e45-9223-757c9959de00",
    queue=Queue.RANKED_SOLO_5X5,
    name="Urgot's Corruptors",
    entries=[]
)


KNOWN_ACCOUNT = Account(
    puuid="Rd3ytVFHq8fHxUe2fZlBuwy9YrjamJbnQbwFgW-cOmyflDQD3WYc890eKlmkb-tAVBIG0fVJA7nZVg",
    gameName="TheTimmy",
    tagLine="EUW"
)

KNOWN_SUMMONER = Summoner(
    id="_dQB7UTlXz1oloSb1XkhilX7ECQoKvHN2BUg-9m4NTPE-3A",
    accountId="fxYYaYbZYisfgAuSS83aTzLPYMw87jVVE-ADKZbxnjYx2g",
    puuid="Rd3ytVFHq8fHxUe2fZlBuwy9YrjamJbnQbwFgW-cOmyflDQD3WYc890eKlmkb-tAVBIG0fVJA7nZVg",
    profileIconId=3378,
    revisionDate=1714518068731,
    summonerLevel=145
)
