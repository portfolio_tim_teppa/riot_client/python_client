from typing import ClassVar, Self
from pydantic import Field

from riot_client.objects.common.constants import Continent
from riot_client.objects.riot_model import RiotModel


class Account(RiotModel):
    ACCOUNTS_URL: ClassVar[str] = f"{RiotModel.URL}/riot/account/v1/accounts"
    puuid: str
    game_name: str | None = Field(alias="gameName")
    tag_line: str | None = Field(alias="tagLine")

    @classmethod
    async def by_riot_id(
        cls,
        game_name: str,
        tag_line: str,
        continent: Continent = Continent.EUROPE
    ) -> Self:
        url = f"{cls.ACCOUNTS_URL}/by-riot-id/{game_name}/{tag_line}".format(target_region=continent)
        account = await cls.send_request(method="GET", url=url)
        return cls(**account)

    @classmethod
    async def by_puuid(cls, puuid: str, continent: Continent = Continent.EUROPE) -> Self:
        url = f"{cls.ACCOUNTS_URL}/by-puuid/{puuid}".format(target_region=continent)
        account = await cls.send_request(method="GET", url=url)
        return cls(**account)
