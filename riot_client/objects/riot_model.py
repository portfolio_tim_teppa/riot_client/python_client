import json
from typing import Any, ClassVar
from pydantic import BaseModel

from riot_client.client.client import RiotClient


class RiotModel(BaseModel):
    CLIENT: ClassVar[RiotClient] = RiotClient()
    URL: ClassVar[str] = "https://{target_region}.api.riotgames.com"

    @classmethod
    async def send_request(cls, *args: Any, **kwargs: Any) -> dict[Any, Any]:
        response = await cls.CLIENT.send_request(*args, **kwargs)
        return json.loads(response.content)
