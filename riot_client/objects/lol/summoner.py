from datetime import datetime
from typing import ClassVar, Self
from pydantic import Field

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.lol_model import LolModel


class Summoner(LolModel):
    SUMMONER_URL: ClassVar[str] = f"{LolModel.LOL_URL}/summoner/v4/summoners"
    account_id: str = Field(alias="accountId")
    profile_icon_id: int = Field(alias="profileIconId")
    revision_date: datetime = Field(alias="revisionDate")
    id: str = Field(alias="id")
    puuid: str = Field(alias="puuid")
    level: int = Field(alias="summonerLevel")

    def __eq__(self, summoner: object) -> bool:
        if not isinstance(summoner, Summoner):
            return super().__eq__(summoner)
        return self.id == summoner.id or self.puuid == summoner.puuid

    @classmethod
    async def by_account(cls, account_id: str, region: Region = Region.EUW1) -> Self:
        url = f"{cls.SUMMONER_URL}/by-account/{account_id}".format(target_region=region)
        summoner = await cls.send_request(method="GET", url=url)
        return cls(**summoner)

    @classmethod
    async def by_id(cls, summoner_id: str, region: Region = Region.EUW1) -> Self:
        url = f"{cls.SUMMONER_URL}/{summoner_id}".format(target_region=region)
        summoner = await cls.send_request(method="GET", url=url)
        return cls(**summoner)

    @classmethod
    async def by_puuid(cls, summoner_puuid: str, region: Region = Region.EUW1) -> Self:
        url = f"{cls.SUMMONER_URL}/by-puuid/{summoner_puuid}".format(target_region=region)
        summoner = await cls.send_request(method="GET", url=url)
        return cls(**summoner)
