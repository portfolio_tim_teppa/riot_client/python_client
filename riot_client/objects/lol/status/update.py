from riot_client.objects.lol.lol_model import LolModel
from riot_client.objects.lol.status.constants import PublishLocation
from riot_client.objects.lol.status.status import Content


class Update(LolModel):
    id: int
    author: str
    publish: bool
    publish_locations: list[PublishLocation]
    translations: list[Content]
    created_at: str
    updated_at: str
