from enum import StrEnum


class MaintenanceStatus(StrEnum):
    SCHEDULED = "scheduled"
    IN_PROGRESS = "in_progress"
    COMPLETE = "complete"


class IncidentSeverity(StrEnum):
    INFO = "info"
    WARNING = "warning"
    CRITICAL = "critical"


class Platform(StrEnum):
    WINDOWS = "windows"
    MACOS = "macos"
    ANDROID = "android"
    IOS = "ios"
    PS4 = "ps4"
    XBOX_ONE = "xbone"
    SWITCH = "switch"


class PublishLocation(StrEnum):
    RIOT_CLIENT = "riotclient"
    RIOT_STATUS = "riotstatus"
    GAME = "game"
