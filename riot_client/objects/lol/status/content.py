from riot_client.objects.lol.lol_model import LolModel


class Content(LolModel):
    locale: str
    content: str
