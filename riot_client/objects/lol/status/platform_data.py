from typing import ClassVar, Self

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.lol_model import LolModel
from riot_client.objects.lol.status.status import Status


class PlatformData(LolModel):
    PLATFORM_DATA_URL: ClassVar[str] = f"{LolModel.LOL_URL}/status/v4/platform-data"
    id: str
    name: str
    locales: list[str]
    maintenances: list[Status]
    incidents: list[Status]

    @classmethod
    async def get(cls, region: Region = Region.EUW1) -> Self:
        url = f"{cls.PLATFORM_DATA_URL}".format(target_region=region)
        platform_data = await cls.send_request(method="GET", url=url)
        return cls(**platform_data)
