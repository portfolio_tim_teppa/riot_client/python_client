from riot_client.objects.lol.lol_model import LolModel
from riot_client.objects.lol.status.constants import (IncidentSeverity,
                                                      MaintenanceStatus,
                                                      Platform)
from riot_client.objects.lol.status.content import Content
from riot_client.objects.lol.status.update import Update


class Status(LolModel):
    id: int
    maintenance_status: MaintenanceStatus | None
    incident_severity: IncidentSeverity | None
    titles: list[Content]
    updates: list[Update]
    created_at: str
    archive_at: str | None
    updated_at: str
    platforms: list[Platform]
