from typing import ClassVar, Self

from pydantic import Field

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.constants import Queue
from riot_client.objects.lol.league_entry import LeagueEntry
from riot_client.objects.lol.lol_model import LolModel


class League(LolModel):
    LEAGUE_URL: ClassVar[str] = f"{LolModel.LOL_URL}/league/v4"
    id: str = Field(alias="leagueId")
    entries: list[LeagueEntry]
    tier: str
    name: str
    queue: Queue

    def __eq__(self, league: object) -> bool:
        if not isinstance(league, League):
            return super().__eq__(league)
        return (
            self.id == league.id
            and self.tier == league.tier
            and self.name == league.name
            and self.queue == league.queue
        )

    @classmethod
    async def by_id(cls, league_id: str, region: Region = Region.EUW1) -> Self:
        url = f"{cls.LEAGUE_URL}/leagues/{league_id}".format(target_region=region)
        league = await cls.send_request(method="GET", url=url)
        return cls(**league)

    @classmethod
    async def challenger(cls, queue: Queue, region: Region = Region.EUW1) -> Self:
        url = f"{cls.LEAGUE_URL}/challengerleagues/by-queue/{queue}".format(target_region=region)
        league = await cls.send_request(method="GET", url=url)
        return cls(**league)

    @classmethod
    async def grand_master(cls, queue: Queue, region: Region = Region.EUW1) -> Self:
        url = f"{cls.LEAGUE_URL}/grandmasterleagues/by-queue/{queue}".format(target_region=region)
        league = await cls.send_request(method="GET", url=url)
        return cls(**league)

    @classmethod
    async def master(cls, queue: Queue, region: Region = Region.EUW1) -> Self:
        url = f"{cls.LEAGUE_URL}/masterleagues/by-queue/{queue}".format(target_region=region)
        league = await cls.send_request(method="GET", url=url)
        return cls(**league)
