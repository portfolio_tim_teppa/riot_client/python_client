from typing import Any, ClassVar, Self

from pydantic import Field
from riot_client.objects.common.constants import Region
from riot_client.objects.lol.constants import Division, Queue, Tier
from riot_client.objects.lol.lol_model import LolModel


class LeagueEntry(LolModel):
    LEAGUE_ENTRIES_URL: ClassVar[str] = f"{LolModel.LOL_URL}/league/v4/entries"
    league_id: str | None = Field(default=None, alias="leagueId")
    summoner_id: str = Field(alias="summonerId")
    queue: Queue | None = Field(default=None, alias="queueType")
    tier: Tier | None = None
    rank: Division
    league_points: int = Field(alias="leaguePoints")
    wins: int
    losses: int
    hot_streak: bool = Field(alias="hotStreak")
    veteran: bool
    fresh_blood: bool = Field(alias="freshBlood")
    inactive: bool
    mini_series: Any = Field(default=None, alias="miniSeries")

    @classmethod
    async def by_summoner(cls, summoner_id: str, region: Region = Region.EUW1) -> list[Self]:
        url = f"{cls.LEAGUE_ENTRIES_URL}/by-summoner/{summoner_id}".format(target_region=region)
        entries = await cls.send_request(method="GET", url=url)
        return [cls(**_entry) for _entry in entries]

    @classmethod
    async def from_(
        cls,
        queue: Queue,
        tier: Tier,
        division: Division,
        region: Region = Region.EUW1
    ) -> list[Self]:
        url = f"{cls.LEAGUE_ENTRIES_URL}/{queue}/{tier}/{division}".format(target_region=region)
        entries = await cls.send_request(method="GET", url=url)
        return [cls(**_entry) for _entry in entries]
