from typing import ClassVar
from riot_client.objects.riot_model import RiotModel


class LolModel(RiotModel):
    LOL_URL: ClassVar[str] = f"{RiotModel.URL}/lol"
