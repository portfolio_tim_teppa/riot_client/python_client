from typing import ClassVar, Self
from pydantic import Field

from riot_client.objects.common.constants import Region
from riot_client.objects.lol.lol_model import LolModel


class ChampionRotations(LolModel):
    CHAMPION_ROTATIONS_URL: ClassVar[str] = f"{LolModel.LOL_URL}/platform/v3/champion-rotations"
    max_new_level_player: int = Field(alias="maxNewPlayerLevel")
    free_champions_ids_for_new_players: list[int] = Field(alias="freeChampionIdsForNewPlayers")
    free_champion_ids: list[int] = Field(alias="freeChampionIds")

    @classmethod
    async def get(cls, region: Region = Region.EUW1) -> Self:
        url = cls.CHAMPION_ROTATIONS_URL.format(target_region=region)
        champion_rotations = await cls.send_request(method="GET", url=url)
        return cls(**champion_rotations)
