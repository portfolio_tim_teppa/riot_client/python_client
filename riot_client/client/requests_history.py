from datetime import datetime, timedelta


class RequestsHistory(set[datetime]):
    time_between_refresh: timedelta

    def __init__(self, time_between_refresh: timedelta) -> None:
        super().__init__()
        self.time_between_refresh = time_between_refresh

    def get_requests_in(self, timedelta_: timedelta) -> set[datetime]:
        now = datetime.now()
        return {
            _request_datetime for _request_datetime in self
            if _request_datetime > now - timedelta_
        }

    def remove_outdated(self, outdating_delay: timedelta | None = None) -> None:
        outdating_delay = outdating_delay or self.time_between_refresh
        outdating_time = datetime.now() - outdating_delay
        for _request_sending_time in self.copy():
            if _request_sending_time < outdating_time:
                self.remove(_request_sending_time)
