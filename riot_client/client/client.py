import asyncio
from datetime import datetime, timedelta
from typing import Any, ClassVar

import requests
from riot_client.client.rate_limit import RateLimit, RateLimits
from riot_client.client.requests_history import RequestsHistory


class RiotClient:
    API_KEY: ClassVar[str] = "RGAPI-20ba2744-e00b-464d-8d3f-c90c98b2e115"
    RATE_LIMITS: ClassVar[RateLimits] = RateLimits([
        RateLimit(time_interval=timedelta(seconds=1), amount_of_requests=20),
        RateLimit(time_interval=timedelta(seconds=120), amount_of_requests=100)
    ])
    _REQUESTS_HISTORY: ClassVar[RequestsHistory] = RequestsHistory(
        time_between_refresh=max(_rate_limit.time_interval for _rate_limit in RATE_LIMITS)
    )

    async def send_request(
        self,
        *args: Any,
        headers: dict[str, str] | None = None,
        **kwargs: Any
    ) -> requests.Response:
        while self.rate_limit_was_reached:
            await asyncio.sleep(self.time_before_next_request_available)
        headers = headers or {}
        headers["X-Riot-Token"] = self.API_KEY
        response = requests.request(*args, headers=headers, **kwargs, timeout=10)
        self._REQUESTS_HISTORY.add(datetime.now())
        return response

    @property
    def rate_limit_was_reached(self) -> bool:
        return self.RATE_LIMITS.were_reached(self._REQUESTS_HISTORY)

    @property
    def time_before_next_request_available(self) -> float:
        time_to_wait: timedelta = timedelta(seconds=0)
        requests_history = list(self._REQUESTS_HISTORY)
        requests_history.sort()
        for _rate_limit in self.RATE_LIMITS:
            if not _rate_limit.was_reached(self._REQUESTS_HISTORY):
                continue
            first_blocking_request_time = requests_history[-_rate_limit.amount_of_requests]
            next_request_available_time = first_blocking_request_time + _rate_limit.time_interval
            _time_to_wait = next_request_available_time - datetime.now()
            time_to_wait = max(time_to_wait, _time_to_wait)
        return time_to_wait.total_seconds()
