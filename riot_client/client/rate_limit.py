from datetime import timedelta

from riot_client.client.requests_history import RequestsHistory


class RateLimit:
    def __init__(self, time_interval: timedelta, amount_of_requests: int) -> None:
        self.time_interval: timedelta = time_interval
        self.amount_of_requests: int = amount_of_requests

    def was_reached(self, requests_history: RequestsHistory) -> bool:
        return len(requests_history.get_requests_in(self.time_interval)) >= self.amount_of_requests


class RateLimits(list[RateLimit]):
    def were_reached(self, requests_history: RequestsHistory) -> bool:
        return any(
            _rate_limit.was_reached(requests_history)
            for _rate_limit in self
        )
